package com.`fun`.tasks.db

import androidx.room.TypeConverter
import com.`fun`.tasks.entity.AlertType

class AlertTypeConverter {
    @TypeConverter
    fun toAlertType(alertType: Int): AlertType {
        return AlertType.toAlertType(alertType)
    }

    @TypeConverter
    fun fromAlertType(alertType: AlertType) = alertType.ordinal
}