package com.`fun`.tasks.db

import androidx.room.*
import com.`fun`.tasks.entity.Task

@Dao
interface TaskDao {

    @Query("SELECT * FROM task")
    fun getAll(): List<Task>

    @Query("SELECT * FROM task WHERE id = :id")
    fun get(id: Int): Task?

    @Query("SELECT * FROM task  WHERE date > 0 ")
    fun getEventTasks(): List<Task>

    @Insert(onConflict = OnConflictStrategy.ABORT)
    fun create(task: Task)

    @Update
    fun update(task: Task)

    @Delete
    fun delete(task: Task)

    @Query("DELETE FROM task WHERE id=:taskId")
    fun delete(taskId: Int)
}