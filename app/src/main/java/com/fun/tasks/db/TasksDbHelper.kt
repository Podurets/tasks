package com.`fun`.tasks.db

import android.content.Context
import android.database.sqlite.SQLiteConstraintException
import com.`fun`.tasks.common.CoroutineScheduler
import com.`fun`.tasks.entity.Task
import com.`fun`.tasks.utils.GenUtils
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class TasksDbHelper {

    fun get(id : Int, context: Context) : Task? = AppDb.db(context).taskDao().get(id)

    fun getEventTasks(context: Context) = AppDb.db(context).taskDao().getEventTasks()

    fun getAll(context: Context, callback: ICallback<List<Task>>) {
        CoroutineScheduler.iOScope.launch {
            val tasks = AppDb.db(context).taskDao().getAll()
            withContext(Dispatchers.Main) {
                callback.onResult(tasks)
            }
        }.start()
    }

    fun delete(taskId: Int, context: Context, callback: ICallback<Boolean>?) {
        CoroutineScheduler.iOScope.launch {
            AppDb.db(context).taskDao().delete(taskId)
            if (callback != null) {
                withContext(Dispatchers.Main) {
                    callback.onResult(true)
                }
            }
        }.start()
    }

    fun save(task: Task, context: Context, callback: ICallback<Boolean>?) {
        CoroutineScheduler.iOScope.launch {
            val result = saveInternal(task, context, callback)
            if (callback != null) {
                withContext(Dispatchers.Main) {
                    callback.onResult(result)
                }
            }
        }.start()
    }

    private fun saveInternal(task: Task, context: Context, callback: ICallback<Boolean>?): Boolean {
        if (task.id == 0) {
            task.id = GenUtils.randomId()
        }
        try {
            AppDb.db(context).taskDao().create(task)
        } catch (e: SQLiteConstraintException) {
            task.id = 0
            return saveInternal(task, context, callback)
        }
        return true
    }

}