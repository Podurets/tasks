package com.`fun`.tasks.db

interface ICallback<T> {

    fun onResult(result : T)

}