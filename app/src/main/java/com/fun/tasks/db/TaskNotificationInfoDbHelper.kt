package com.`fun`.tasks.db

import android.content.Context
import com.`fun`.tasks.common.CoroutineScheduler
import com.`fun`.tasks.entity.TaskNotificationInfo
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class TaskNotificationInfoDbHelper {

    fun get(id: Int, context: Context): TaskNotificationInfo? = AppDb.db(context).taskNotificationDao().get(id)

    fun getByTask(taskId: Int, context: Context): TaskNotificationInfo? = AppDb.db(context).taskNotificationDao().getByTask(taskId)

    fun save(taskNotificationInfo: TaskNotificationInfo, context: Context, callback: ICallback<Boolean>?) {
        CoroutineScheduler.iOScope.launch {
            if (taskNotificationInfo.id == 0) {
                AppDb.db(context).taskNotificationDao().create(taskNotificationInfo)
            } else {
                AppDb.db(context).taskNotificationDao().update(taskNotificationInfo)
            }

            if (callback != null) {
                withContext(Dispatchers.Main) {
                    callback.onResult(true)
                }
            }
        }.start()
    }

}