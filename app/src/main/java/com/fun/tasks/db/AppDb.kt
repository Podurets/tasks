package com.`fun`.tasks.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.`fun`.tasks.entity.Task
import com.`fun`.tasks.entity.TaskNotificationInfo

@Database(entities = [Task::class, TaskNotificationInfo::class], version = 1)
abstract class AppDb : RoomDatabase() {

    abstract fun taskDao(): TaskDao
    abstract fun taskNotificationDao(): TaskNotificationInfoDao

    companion object {
        private var DB_LINK: AppDb? = null

        fun init(context: Context){
            val db = DB_LINK
            if(db == null || !db.isOpen){
                DB_LINK = createDbInstance(context)
            }
        }

        private fun createDbInstance(context: Context) =
            Room.databaseBuilder(context.applicationContext, AppDb::class.java, "app.db").build()

        fun db(context: Context): AppDb {
            var db = DB_LINK
            if (db == null) {
                synchronized(AppDb::class) {
                    db = createDbInstance(context)
                    DB_LINK = db
                }
            }
            return db as AppDb
        }
    }

}