package com.`fun`.tasks.db

import androidx.room.TypeConverter
import com.`fun`.tasks.entity.TaskPeriod

class TaskPeriodConverter {

    @TypeConverter
    fun toTaskPeriod(period: Int): TaskPeriod {
        return TaskPeriod.toTaskPeriod(period)
    }

    @TypeConverter
    fun fromTaskPeriod(taskPeriod: TaskPeriod) = taskPeriod.ordinal
}