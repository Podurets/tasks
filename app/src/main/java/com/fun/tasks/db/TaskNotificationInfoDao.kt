package com.`fun`.tasks.db

import androidx.room.*
import com.`fun`.tasks.entity.TaskNotificationInfo

@Dao
interface TaskNotificationInfoDao {

    @Query("SELECT * FROM notificationInfo WHERE id = :id")
    fun get(id: Int): TaskNotificationInfo?

    @Query("SELECT * FROM notificationInfo WHERE task_id = :taskId")
    fun getByTask(taskId: Int): TaskNotificationInfo?

    @Query("SELECT * FROM notificationInfo")
    fun getAll(): List<TaskNotificationInfo>

    @Insert(onConflict = OnConflictStrategy.ABORT)
    fun create(taskNotificationInfo: TaskNotificationInfo)

    @Update
    fun update(taskNotificationInfo: TaskNotificationInfo)

    @Delete
    fun delete(taskNotificationInfo: TaskNotificationInfo)

}