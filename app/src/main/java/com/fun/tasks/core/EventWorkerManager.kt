package com.`fun`.tasks.core

import android.content.Context
import androidx.work.*
import androidx.work.WorkRequest.Builder
import com.`fun`.tasks.Const
import com.`fun`.tasks.common.CoroutineScheduler
import com.`fun`.tasks.common.Lg
import com.`fun`.tasks.db.TasksDbHelper
import com.`fun`.tasks.entity.Task
import com.`fun`.tasks.entity.TaskPeriod
import com.`fun`.tasks.utils.GenUtils
import kotlinx.coroutines.launch
import java.util.*
import java.util.concurrent.TimeUnit


object EventWorkerManager {

    const val minimalEventDelay = 60000 * 15 /// 15 min

    private val constraintBuilder: Constraints.Builder by lazy {
        Constraints.Builder()
            .setRequiredNetworkType(NetworkType.NOT_REQUIRED)
            .setRequiresBatteryNotLow(false)
            .setRequiresStorageNotLow(false)
            .setRequiresCharging(false)
    }

    private fun buildWorkerTag(id: Int) = "task_$id"

    fun verifyEvents(context: Context) {
        CoroutineScheduler.iOScope.launch {
            val tasks = TasksDbHelper().getEventTasks(context)
            if (tasks.isEmpty()) {
                cancelAllTaskEvents(context)
            } else {
                val workerManager  = WorkManager.getInstance(context)
                for (task in tasks) {
                    applyTaskEvent(task, workerManager, context)
                }
            }
        }
    }

    fun applyTaskEvent(task : Task, context: Context) {
        applyTaskEvent(task, WorkManager.getInstance(context), context)
    }

    fun cancelTaskEvent(id: Int, context: Context) {
        cancelTaskEvent(id, WorkManager.getInstance(context))
    }

    private fun cancelAllTaskEvents(context: Context){
        WorkManager.getInstance(context).cancelAllWork()
    }

    private fun cancelTaskEvent(id: Int, workerManager: WorkManager) {
        cancelTaskEvent(
            buildWorkerTag(
                id
            ), workerManager
        )
    }

    private fun cancelTaskEvent(tag: String, workerManager: WorkManager) {
        workerManager.cancelAllWorkByTag(tag)
    }

    private fun taskStartDelay(task : Task) : Long {
        val currentTime = System.currentTimeMillis()
        val delay = task.date - currentTime
        if (delay > 0) {
            return delay
        } else {
            val  calendar = Calendar.getInstance()
            calendar.timeInMillis = task.date
            when(task.taskPeriod){
                TaskPeriod.Daily -> {
                    calendar.add(Calendar.DAY_OF_YEAR, 1)
                    return calendar.timeInMillis - currentTime
                }
                TaskPeriod.Weekly -> {
                    calendar.add(Calendar.DAY_OF_YEAR, 7)
                    return calendar.timeInMillis - currentTime
                }
                TaskPeriod.Monthly -> {
                    calendar.add(Calendar.MONTH, 1)
                    return calendar.timeInMillis - currentTime
                }
                TaskPeriod.Yearly -> {
                    calendar.add(Calendar.YEAR, 1)
                    return calendar.timeInMillis - currentTime
                }
                else -> {
                    return -1
                }
            }
        }
    }

    private fun applyTaskEvent(task: Task, workerManager: WorkManager, context: Context) {
        val tag = buildWorkerTag(task.id)
        if (task.date > 0) {
            val delay = taskStartDelay(task)
            if (delay > 0) {

                if (minimalEventDelay >= delay) {
                    workerManager.cancelAllWorkByTag(tag)
                    NotificationEventManager.displayTaskNotification(task, context)
                    return
                }

                val works = workerManager.getWorkInfosByTag(tag).get()
                if (works != null && works.size > 0) {
                    if (works.size == 1) {
                        val workInfo = works[0]
                        if (workInfo.state == WorkInfo.State.ENQUEUED) {
                            Lg.e("skipped create work for task: ${task.title}, already available")
                            return
                        }
                    } else {
                        workerManager.cancelAllWorkByTag(tag)
                    }
                }
                val oneTimeWorkRequest = OneTimeWorkRequest.Builder(TaskEventWorker::class.java)
                    .setInitialDelay(delay, TimeUnit.MILLISECONDS)
                prepareWorkRequest(task, tag, oneTimeWorkRequest)
                workerManager.enqueueUniqueWork(tag, ExistingWorkPolicy.REPLACE, oneTimeWorkRequest.build())
                val time = GenUtils.formatTaskByTimeFormat(System.currentTimeMillis() + delay)
                Lg.e("created work for task: ${task.title} for date: $time")
            } else {
                cancelTaskEvent(tag, workerManager)
            }
        } else {
            cancelTaskEvent(tag, workerManager)
        }
    }

    private fun <W : WorkRequest, B : Builder<*, W>> prepareWorkRequest(
        task: Task,
        tag: String,
        workRequest: Builder<B, W>
    ) {
        workRequest.setConstraints(constraintBuilder.build())
        workRequest.addTag(tag)
        val data = Data.Builder()
            .putInt(Const.ID_KEY, task.id)
            .putLong(Const.TIME_KEY, task.date)
            .putInt(Const.PERIOD_KEY, task.taskPeriod.toIntOrder())
        workRequest.setInputData(data.build())
    }

}