package com.`fun`.tasks.core

import android.content.Context
import androidx.work.CoroutineWorker
import androidx.work.WorkerParameters
import com.`fun`.tasks.Const
import com.`fun`.tasks.common.Lg
import com.`fun`.tasks.db.TaskNotificationInfoDbHelper
import com.`fun`.tasks.db.TasksDbHelper
import com.`fun`.tasks.entity.TaskNotificationInfo
import kotlinx.coroutines.async
import kotlinx.coroutines.coroutineScope

class TaskEventWorker(context: Context, params: WorkerParameters) : CoroutineWorker(context, params) {


    override suspend fun doWork(): Result = coroutineScope {
        Lg.e("TaskEventWorker doWork")
        val data = inputData
        val id = data.getInt(Const.ID_KEY, 0)
        async {
            val task = TasksDbHelper().get(id, applicationContext)
            var taskNotificationInfo: TaskNotificationInfo?
            if (task != null && task.date > 0) {
                taskNotificationInfo = TaskNotificationInfoDbHelper().getByTask(task.id, applicationContext)
                if (taskNotificationInfo != null && taskNotificationInfo.isDisplayed) {
                    return@async
                }
                if(taskNotificationInfo == null){
                    taskNotificationInfo = TaskNotificationInfo(task.id)
                }
                taskNotificationInfo.isDisplayed = true
                TaskNotificationInfoDbHelper().save(taskNotificationInfo, applicationContext, null)
                NotificationEventManager.displayTaskNotification(task, applicationContext)
            }
        }
        Result.success()
    }
}