package com.`fun`.tasks.core

import android.content.Context
import android.os.Handler
import android.os.Looper
import android.os.Message
import android.util.Log
import com.`fun`.tasks.entity.Task

object NotificationEventManager {

    private const val TRIGGER_SHOW_NOTIFICATION = 10
    private const val TRIGGER_SHOW_NOTIFICATION_DELAY = 2000L
    private val tasksStack = ArrayList<Task>()
    private val delayHandler = Handler(Looper.getMainLooper(), Handler.Callback {
        if (it.what == TRIGGER_SHOW_NOTIFICATION) {
            if(it.obj is Task){
                tryDisplayNotificationInternal(it.obj as Task)
            }
        }
        true
    })

    fun displayTaskNotification(task: Task, context: Context) {
        tasksStack.add(task)
        triggerToShowNotification(task, context)
    }

    fun displayTaskNotification(tasks: ArrayList<Task>, context: Context) {
        tasksStack.addAll(tasks)
        for (task in tasks){
            triggerToShowNotification(task, context)
        }
    }

    private fun triggerToShowNotification(task: Task, context: Context) {
        val message = Message.obtain(delayHandler, TRIGGER_SHOW_NOTIFICATION)
        message.obj = task
        message.sendToTarget()
    }

    private fun tryDisplayNotificationInternal(task: Task){
        Log.e("TTTTT", "Got task: "+task.id)
    }

}