package com.`fun`.tasks.utils

import android.app.Activity
import android.content.ActivityNotFoundException
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.speech.RecognizerIntent
import java.util.*


class VoiceInputManager {

    private var languageCallback : ResultCallback? = null

    fun startVoice(activity: Activity, promptText: String? = null, locale: Locale = Locale.getDefault()): Boolean {
        val intent = Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH)
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM)
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, locale)
        if (promptText != null) {
            intent.putExtra(RecognizerIntent.EXTRA_PROMPT, promptText)
        }
        try {
            activity.startActivityForResult(intent, REQUEST_CODE)
        } catch (a: ActivityNotFoundException) {
            return false
        }
        return true
    }

    fun loadSTTSupportedLangs(context: Context, callback: ResultCallback) {
        languageCallback = callback
        val detailsIntent = RecognizerIntent.getVoiceDetailsIntent(context)
        if (detailsIntent != null) {
            context.sendOrderedBroadcast(
                detailsIntent, null, receiver, null, Activity.RESULT_OK, null, null
            )
        }
    }

    fun processActivityResult(requestCode: Int, resultCode: Int, intent: Intent?, callback: ResultCallback) {
        if (requestCode == REQUEST_CODE && resultCode == Activity.RESULT_OK && intent != null) {
            val result = intent.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS)
            callback.onResult(result)
        }
    }

    private val receiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            val results = getResultExtras(true)
            var languagePreference : String ? = null
            if (results.containsKey(RecognizerIntent.EXTRA_LANGUAGE_PREFERENCE)) {
                languagePreference = results.getString(RecognizerIntent.EXTRA_LANGUAGE_PREFERENCE)
            }
            var items : ArrayList<String>? = null
            if (results.containsKey(RecognizerIntent.EXTRA_SUPPORTED_LANGUAGES)) {
                items = results.getStringArrayList(RecognizerIntent.EXTRA_SUPPORTED_LANGUAGES)
            }
            languageCallback?.onResult(items)
        }
    }

    interface ResultCallback {
        fun onResult(result: ArrayList<String>?)
    }


    companion object {
        const val REQUEST_CODE = 100
    }

}