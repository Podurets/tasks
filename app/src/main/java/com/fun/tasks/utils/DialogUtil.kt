package com.`fun`.tasks.utils

import android.content.Context
import android.content.DialogInterface
import androidx.appcompat.app.AlertDialog
import com.`fun`.tasks.R

object DialogUtil {

    fun buildDialog(
        context: Context, title: String? = null, items: Array<String>,
        checkedIndex: Int = 0,
        clickListener: DialogInterface.OnClickListener
    ): AlertDialog {
        val builder = AlertDialog.Builder(context)
        builder.setTitle(title)
        builder.setSingleChoiceItems(items, checkedIndex, clickListener)
        builder.setNegativeButton("Cancel", null)
        return builder.create()
    }

    fun showDialog(
        context: Context, confirmClickListener: DialogInterface.OnClickListener,
        cancelClickListener: DialogInterface.OnClickListener,
        message: String
    ): AlertDialog {
        val builder = AlertDialog.Builder(context)
        builder.setTitle(null)
        builder.setMessage(message)
        builder.setPositiveButton(context.getString(R.string.yes), confirmClickListener)
        builder.setNegativeButton(context.getString(R.string.no), cancelClickListener)
        return builder.create()
    }

}