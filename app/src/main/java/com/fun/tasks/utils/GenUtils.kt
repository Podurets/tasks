package com.`fun`.tasks.utils

import com.`fun`.tasks.entity.Task
import java.text.SimpleDateFormat
import java.util.*

object GenUtils {

    val SUPPORTED_APP_LANGS: Array<String> = arrayOf("ru-RU", "uk-UA", "en-US")

    private val dateFormat = SimpleDateFormat("dd.MM.yyyy", Locale.ENGLISH)
    private val timeDateFormat = SimpleDateFormat("HH:mm, dd.MM.yyyy", Locale.ENGLISH)

    fun randomId() = Random().nextInt()

    fun formatTaskDate(task: Task): String =
        if (task.isUseTime) timeDateFormat.format(task.date) else dateFormat.format(task.date)

    fun formatTaskByTimeFormat(task: Task): String = timeDateFormat.format(task.date)

    fun formatTaskByTimeFormat(time: Long): String = timeDateFormat.format(time)

    fun formatTaskByDateFormat(task: Task): String = dateFormat.format(task.date)
}