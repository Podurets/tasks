package com.`fun`.tasks.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.Index
import androidx.room.PrimaryKey

@Entity(tableName = "notificationInfo", indices = [Index(value = ["task_id"], unique = true)])
class TaskNotificationInfo {

    constructor(taskId : Int){
        this.taskId = taskId
    }

    @PrimaryKey(autoGenerate = true)
    var id: Int = 0
    @ColumnInfo(name = "displayed")
    var isDisplayed = false
    @ColumnInfo(name = "task_id")
    val taskId : Int

}