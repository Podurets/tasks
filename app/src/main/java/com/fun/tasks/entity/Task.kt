package com.`fun`.tasks.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.TypeConverters
import com.`fun`.tasks.db.AlertTypeConverter
import com.`fun`.tasks.db.TaskPeriodConverter

@Entity
class Task {

    @PrimaryKey(autoGenerate = false)
    var id: Int = 0
    @ColumnInfo(name = "title")
    var title: String = ""
    @ColumnInfo(name = "desc")
    var description: String? = null
    @ColumnInfo(name = "period")
    @TypeConverters(TaskPeriodConverter::class)
    var taskPeriod: TaskPeriod = TaskPeriod.None
    @ColumnInfo(name = "alert")
    @TypeConverters(AlertTypeConverter::class)
    var alertType: AlertType = AlertType.Vibro
    @ColumnInfo(name = "date")
    var date: Long = 0
    @ColumnInfo(name = "isTimeUse")
    var isUseTime: Boolean = false
    @ColumnInfo(name = "isEnabledEvent")
    var isEnabledEvent: Boolean = false
}