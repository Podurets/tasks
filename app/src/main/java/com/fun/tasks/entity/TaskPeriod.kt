package com.`fun`.tasks.entity

enum class TaskPeriod {
    None, Daily, Weekly, Monthly, Yearly;

    companion object {

        fun toTaskPeriod(periodOrder: Int): TaskPeriod {
            if (values().size > periodOrder) {
                return values()[periodOrder]
            }
            return None
        }
    }

    fun toIntOrder()  = ordinal

}