package com.`fun`.tasks.entity

enum class AlertType {

    Vibro, Sound;

    companion object {
        fun toAlertType(alertType: Int): AlertType {
            if (values().size > alertType) {
                return values()[alertType]
            }
            return Vibro
        }
    }

}