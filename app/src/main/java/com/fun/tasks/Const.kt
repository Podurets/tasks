package com.`fun`.tasks

object Const {

    const val ID_KEY = "id"
    const val TIME_KEY = "time"
    const val PERIOD_KEY = "period"

}