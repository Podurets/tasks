package com.`fun`.tasks.receiver

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import com.`fun`.tasks.core.EventWorkerManager

class BootReceiver : BroadcastReceiver() {

    override fun onReceive(context: Context?, intent: Intent?) {
        if (context != null) {
            EventWorkerManager.verifyEvents(context)
        }
    }
}