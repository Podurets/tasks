package com.`fun`.tasks.modules.main

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.`fun`.tasks.R
import com.`fun`.tasks.common.IClickListener
import com.`fun`.tasks.entity.AlertType
import com.`fun`.tasks.entity.Task
import com.`fun`.tasks.utils.GenUtils
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.task_item.view.*

class TasksAdapter : BaseArrayAdapter<Task, TasksAdapter.TasksHolder>() {

    var longClickListener : IClickListener? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) : TasksHolder {
        val holder = TasksHolder(LayoutInflater.from(parent.context), parent)
        holder.setLongClickListener(longClickListener)
        return holder
    }


    override fun onBindViewHolder(holder: TasksHolder, position: Int) {
        holder.bind(items[position])
        holder.setLongClickListener(longClickListener)
    }

    inner class TasksHolder(inflater: LayoutInflater, parent: ViewGroup) :
        RecyclerView.ViewHolder(inflater.inflate(R.layout.task_item, parent, false)) {

        private var longClickListener : IClickListener? = null

        fun bind(task: Task) {
            itemView.text_title.text = task.title
            itemView.text_description.text = task.description
            if (task.date > 0) {
                Glide.with(itemView.icon_view).load(R.drawable.ic_event_done).dontAnimate().into(itemView.icon_view)
                itemView.text_date.visibility = View.VISIBLE
                itemView.text_date.text = GenUtils.formatTaskDate(task)
            } else {
                itemView.text_date.visibility = View.GONE
                Glide.with(itemView.icon_view).load(R.drawable.ic_event_default).dontAnimate().into(itemView.icon_view)
            }
            val resId = if (task.alertType == AlertType.Vibro) R.drawable.ic_vibration else R.drawable.ic_volume
            Glide.with(itemView.alert_iv).load(resId).dontAnimate().into(itemView.alert_iv)
        }

        fun setLongClickListener(clickListener: IClickListener?) {
            longClickListener = clickListener
            if (longClickListener == null) {
                itemView.setOnLongClickListener(null)
            } else {
                itemView.setOnLongClickListener(viewLongClickListener)
            }
        }

        private val viewLongClickListener = View.OnLongClickListener {
            val position = adapterPosition
            if (position >= 0) {
                longClickListener?.onClick(position)
            }
            true
        }

    }

}