package com.`fun`.tasks.modules.main

import androidx.recyclerview.widget.RecyclerView

abstract class BaseArrayAdapter<M, T : RecyclerView.ViewHolder> : RecyclerView.Adapter<T>() {

    protected val items: ArrayList<M> = ArrayList()

    fun applyItems(items: List<M>) {
        this.items.clear()
        this.items.addAll(items)
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int {
        return items.size
    }

    fun getItem(index: Int): M? {
        if (items.size > index && index >= 0) {
            return items[index]
        }
        return null
    }

}