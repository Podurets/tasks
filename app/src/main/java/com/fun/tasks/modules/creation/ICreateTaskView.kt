package com.`fun`.tasks.modules.creation

import android.content.Context
import com.`fun`.tasks.modules.common.IView

interface ICreateTaskView : IView {

    fun onError(text: String)

    fun onSuccessSave()

    fun context(): Context

    fun showDatePicker(date: Long)

    fun showTimePicker(time: Long)

    fun drawDate(text : String)

    fun drawTime(text : String)

}