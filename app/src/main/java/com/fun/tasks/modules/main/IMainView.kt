package com.`fun`.tasks.modules.main

import android.content.Context
import com.`fun`.tasks.entity.Task
import com.`fun`.tasks.modules.common.IView

interface IMainView : IView {

    fun drawTasks(task: List<Task>)
    fun context() : Context

}