package com.`fun`.tasks.modules.creation

import com.`fun`.tasks.core.EventWorkerManager
import com.`fun`.tasks.db.ICallback
import com.`fun`.tasks.db.TasksDbHelper
import com.`fun`.tasks.entity.AlertType
import com.`fun`.tasks.entity.Task
import com.`fun`.tasks.entity.TaskPeriod
import com.`fun`.tasks.modules.common.BaseViewModel
import com.`fun`.tasks.utils.GenUtils
import java.util.*

class CreateTaskViewModel : BaseViewModel<ICreateTaskView>(), ICreateTaskViewModel {

    private val task = Task()

    override fun notifySave() {
        val view = view()
        if (view != null) {
            if (task.title.isBlank()) {
                view.onError("Missing title for task")
                return
            }
            TasksDbHelper().save(task, view.context(), object : ICallback<Boolean> {
                override fun onResult(result: Boolean) {
                    val viewLink = view()
                    if (viewLink != null) {
                        if (result) {
                            EventWorkerManager.applyTaskEvent(task, viewLink.context())
                            viewLink.onSuccessSave()
                        } else {
                            viewLink.onError("Cannot save task")
                        }
                    }
                }

            })
        }
    }

    override fun applyTaskName(name: String) {
        task.title = name
    }

    override fun applyTaskDescription(description: String?) {
        task.description = description
    }

    override fun applyPeriod(taskPeriod: TaskPeriod) {
        task.taskPeriod = taskPeriod
    }

    override fun applyDate(date: Long) {
        task.date = date
        notifyViewOnChangedDate()
    }

    override fun applyTime(time: Long) {
        task.isUseTime = true
        task.date = time
        notifyViewOnChangedDate()
    }

    private fun notifyViewOnChangedDate(){
        view()?.drawDate(GenUtils.formatTaskByDateFormat(task))
        view()?.drawTime(GenUtils.formatTaskByTimeFormat(task))
    }

    override fun applySettings(alertType: AlertType) {
        task.alertType = alertType
    }

    override fun onClickDatePicker() {
        val view = view()
        if (view != null) {
            val date = if (task.date > 0) task.date else Calendar.getInstance().timeInMillis
            view.showDatePicker(date)
        }
    }

    override fun onClickTimePicker() {
        val view = view()
        if (view != null) {
            val date = if (task.date > 0) task.date else Calendar.getInstance().timeInMillis
            view.showTimePicker(date)
        }
    }

}