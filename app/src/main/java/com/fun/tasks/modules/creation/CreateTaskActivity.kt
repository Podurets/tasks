package com.`fun`.tasks.modules.creation

import android.app.DatePickerDialog
import android.app.TimePickerDialog
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import androidx.lifecycle.ViewModelProviders
import com.`fun`.tasks.R
import com.`fun`.tasks.entity.AlertType
import com.`fun`.tasks.entity.TaskPeriod
import com.`fun`.tasks.modules.common.ViewModelActivity
import com.`fun`.tasks.modules.creation.common.AlertSettingsSpinnerAdapter
import com.`fun`.tasks.modules.creation.common.PeriodSpinnerAdapter
import com.`fun`.tasks.utils.DialogUtil
import com.`fun`.tasks.utils.GenUtils
import com.`fun`.tasks.utils.VoiceInputManager
import kotlinx.android.synthetic.main.create_task_layout.*
import java.util.*
import kotlin.collections.ArrayList


class CreateTaskActivity : ViewModelActivity<ICreateTaskView, CreateTaskViewModel>(), ICreateTaskView {

    private val calendar = Calendar.getInstance()
    private val voiceInputManager = VoiceInputManager()
    private var isTitleMode = false
    private var supportedLangs: Array<String> = Array<String>(0) { ""}

    override fun initViewModel() = ViewModelProviders.of(this).get(CreateTaskViewModel::class.java)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.create_task_layout)
        period_spinner.adapter = PeriodSpinnerAdapter(TaskPeriod.values())
        alert_spinner.adapter = AlertSettingsSpinnerAdapter(AlertType.values())
        date_picker_tv.setOnClickListener {
            viewModel().onClickDatePicker()
        }
        time_picker_tv.setOnClickListener {
            viewModel().onClickTimePicker()
        }
        voice_name_ic.setOnClickListener {
            isTitleMode = true
            voiceInputManager.startVoice(this)
        }

        voice_desc_ic.setOnClickListener {
            isTitleMode = false
            voiceInputManager.startVoice(this)
        }
        viewModel().bind(this)
        voiceInputManager.loadSTTSupportedLangs(this, object : VoiceInputManager.ResultCallback {
            override fun onResult(result: ArrayList<String>?) {
                supportedLangs = buildSupportedLangsList(result)
            }
        })
    }

    private fun buildSupportedLangsList(result: ArrayList<String>?): Array<String> {
        if (result != null) {
            val items = ArrayList<String>()
            for (item in result) {
                for (value in GenUtils.SUPPORTED_APP_LANGS) {
                    if (value.contains(item)) {
                        items.add(item)
                        break
                    }
                }
                if(items.size == GenUtils.SUPPORTED_APP_LANGS.size){
                    break
                }
            }
            if (items.size > 0) {
                return items.toTypedArray()
            }
        }
        return Array(1) { "" }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.create_task_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.add -> {
                val viewModel = viewModel()
                val title = task_name_ed.text?.toString() ?: ""
                viewModel.applyTaskName(title)
                val description = task_description_ed.text?.toString() ?: ""
                viewModel.applyTaskDescription(description)
                val period = period_spinner.selectedItem
                if (period is TaskPeriod) {
                    viewModel.applyPeriod(period)
                }
                val alert = alert_spinner.selectedItem
                if (alert is AlertType) {
                    viewModel.applySettings(alert)
                }
                viewModel.notifySave()
                return true
            }
            R.id.voice_settings -> {
                DialogUtil.buildDialog(this, null, supportedLangs, 0,
                    DialogInterface.OnClickListener { dialog, which ->

                    }).show()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        voiceInputManager.processActivityResult(
            requestCode,
            resultCode,
            data,
            object : VoiceInputManager.ResultCallback {
                override fun onResult(result: ArrayList<String>?) {
                    if (result != null && result.size > 0) {
                        val item = result[0]
                        processVoiceText(item)
                    }
                }
            })
    }

    private fun processVoiceText(text: String) {
        if (!text.isBlank()) {
            if (isTitleMode) {
                task_name_ed.setText(text)
                viewModel().applyTaskName(text)
            } else {
                task_description_ed.setText(text)
                viewModel().applyTaskDescription(text)
            }
        }
    }

    override fun showDatePicker(date: Long) {
        calendar.timeInMillis = date
        val datePicker = DatePickerDialog(
            this, DatePickerDialog.OnDateSetListener { _, year, month, dayOfMonth ->
                calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth)
                calendar.set(Calendar.MONTH, month)
                calendar.set(Calendar.YEAR, year)
                viewModel().applyDate(calendar.timeInMillis)
            }, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH)
        )
        datePicker.setTitle(R.string.select_date)
        datePicker.show()
    }

    override fun showTimePicker(time: Long) {
        calendar.timeInMillis = time
        val timePicker = TimePickerDialog(
            this,
            TimePickerDialog.OnTimeSetListener { _, selectedHour, selectedMinute ->
                calendar.set(Calendar.MINUTE, selectedMinute)
                calendar.set(Calendar.HOUR_OF_DAY, selectedHour)
                viewModel().applyTime(calendar.timeInMillis)
            }, calendar.get(Calendar.HOUR_OF_DAY), calendar.get(Calendar.MINUTE), true
        )
        timePicker.setTitle(R.string.select_time)
        timePicker.show()
    }

    override fun drawTime(text: String) {
        time_picker_tv.text = text
    }

    override fun drawDate(text: String) {
        date_picker_tv.text = text
    }

    override fun onError(text: String) {
        Toast.makeText(this, text, Toast.LENGTH_LONG).show()
    }

    override fun onSuccessSave() {
        Toast.makeText(applicationContext, getString(R.string.saved), Toast.LENGTH_LONG).show()
        finish()
    }

    override fun context(): Context = applicationContext

}