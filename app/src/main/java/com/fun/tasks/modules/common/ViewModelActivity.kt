package com.`fun`.tasks.modules.common

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity

abstract class ViewModelActivity<V : IView, VM : BaseViewModel<V>> : AppCompatActivity() {

    private lateinit var vm : VM

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        vm = initViewModel()
    }

    override fun onDestroy() {
        super.onDestroy()
        viewModel().unbind()
    }

    protected fun viewModel() = vm

    abstract fun initViewModel() : VM


}