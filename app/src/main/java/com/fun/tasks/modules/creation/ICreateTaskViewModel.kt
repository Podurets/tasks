package com.`fun`.tasks.modules.creation

import com.`fun`.tasks.entity.AlertType
import com.`fun`.tasks.entity.TaskPeriod
import com.`fun`.tasks.modules.common.IViewModel

interface ICreateTaskViewModel : IViewModel {

    fun applyTaskName(name: String)
    fun applyTaskDescription(description: String?)
    fun applyPeriod(taskPeriod: TaskPeriod)
    fun applyDate(date: Long)
    fun applyTime(time: Long)
    fun applySettings(alertType: AlertType)
    fun notifySave()

    fun onClickDatePicker()
    fun onClickTimePicker()

}