package com.`fun`.tasks.modules.main

import com.`fun`.tasks.db.ICallback
import com.`fun`.tasks.db.TasksDbHelper
import com.`fun`.tasks.entity.Task
import com.`fun`.tasks.modules.common.BaseViewModel

class MainViewModel : BaseViewModel<IMainView>(), IMainViewModel {

    private val dbRepo = TasksDbHelper()

    override fun loadTasks() {
        val view = view()
        if (view != null) {
            dbRepo.getAll(view.context(), object : ICallback<List<Task>> {
                override fun onResult(result: List<Task>) {
                    view()?.drawTasks(result)
                }
            })
        }
    }

    override fun deleteTask(id: Int) {
        val view = view()
        if (view != null) {
            dbRepo.delete(id, view.context(), object : ICallback<Boolean> {
                override fun onResult(result: Boolean) {
                    loadTasks()
                }
            })
        }
    }


}