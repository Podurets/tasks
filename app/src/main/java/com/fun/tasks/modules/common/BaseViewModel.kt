package com.`fun`.tasks.modules.common

import androidx.lifecycle.ViewModel

abstract class BaseViewModel<V : IView> : ViewModel(), IViewModel {

    private var view: V? = null

    fun bind(view: V) {
        this.view = view
    }

    fun unbind() {
        view = null
    }

    fun view() = view

    override fun onCleared() {
        super.onCleared()
        if (view != null) {
            view = null
        }
    }

}