package com.`fun`.tasks.modules.main

import com.`fun`.tasks.modules.common.IViewModel

interface IMainViewModel : IViewModel {

    fun loadTasks()

    fun deleteTask(id : Int)

}