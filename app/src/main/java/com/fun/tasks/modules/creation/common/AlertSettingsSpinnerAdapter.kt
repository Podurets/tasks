package com.`fun`.tasks.modules.creation.common

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.`fun`.tasks.R
import com.`fun`.tasks.common.DefaultSpinnerHolder
import com.`fun`.tasks.common.GenericSpinnerAdapter
import com.`fun`.tasks.entity.AlertType

class AlertSettingsSpinnerAdapter(private val items : Array<AlertType>) : GenericSpinnerAdapter() {

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val holder: DefaultSpinnerHolder
        val itemView: View
        if (convertView == null) {
            itemView = LayoutInflater.from(parent.context).inflate(R.layout.dropdown_item, parent, false)
            holder = DefaultSpinnerHolder(itemView.findViewById(R.id.text_title))
        } else {
            holder = convertView.tag as DefaultSpinnerHolder
            itemView = convertView
        }
        itemView.tag = holder
        holder.titleView.text = items[position].name
        return itemView
    }

    override fun getItem(position: Int): Any {
        return items[position]
    }

    override fun getCount(): Int {
        return items.size
    }
}