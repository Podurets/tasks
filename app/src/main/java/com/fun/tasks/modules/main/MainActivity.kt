package com.`fun`.tasks.modules.main

import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.`fun`.tasks.R
import com.`fun`.tasks.common.IClickListener
import com.`fun`.tasks.entity.Task
import com.`fun`.tasks.modules.common.ViewModelActivity
import com.`fun`.tasks.modules.creation.CreateTaskActivity
import com.`fun`.tasks.utils.DialogUtil
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : ViewModelActivity<IMainView, MainViewModel>(), IMainView {
    override fun context(): Context = applicationContext

    override fun drawTasks(task: List<Task>) {
        tasksAdapter.applyItems(task)
    }

    private val tasksAdapter = TasksAdapter()

    override fun initViewModel() = ViewModelProviders.of(this).get(MainViewModel::class.java)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        recycler.layoutManager = LinearLayoutManager(this)
        recycler.adapter = tasksAdapter
        tasksAdapter.longClickListener = object : IClickListener {
            override fun onClick(position: Int) {
                val task = tasksAdapter.getItem(position)
                if (task != null) {
                    DialogUtil.showDialog(
                        this@MainActivity,
                        DialogInterface.OnClickListener { dialog, which ->
                            viewModel().deleteTask(task.id)
                        },
                        DialogInterface.OnClickListener { dialog, which ->

                        },
                        getString(R.string.delete_task_question)
                    ).show()
                }
            }
        }
        viewModel().bind(this)
    }

    override fun onStart() {
        super.onStart()
        viewModel().loadTasks()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.add_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.add -> {
                startActivity(Intent(this@MainActivity, CreateTaskActivity::class.java))
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

}
