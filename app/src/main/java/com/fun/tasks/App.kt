package com.`fun`.tasks

import android.app.Application
import android.content.ComponentName
import android.content.pm.PackageManager
import com.`fun`.tasks.core.EventWorkerManager
import com.`fun`.tasks.db.AppDb
import com.`fun`.tasks.receiver.BootReceiver

class App : Application() {

    override fun onCreate() {
        super.onCreate()
        enableBootReceiver()
        AppDb.init(this)
        EventWorkerManager.verifyEvents(this)
    }

    private fun enableBootReceiver() {
        packageManager.setComponentEnabledSetting(
            ComponentName(this, BootReceiver::class.java),
            PackageManager.COMPONENT_ENABLED_STATE_DISABLED,
            PackageManager.DONT_KILL_APP
        )
    }

}