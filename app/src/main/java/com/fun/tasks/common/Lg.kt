package com.`fun`.tasks.common

import android.util.Log

object Lg {

    var ENABLED = true
    private val tag : String  by lazy {"logger"}


    fun e(tag: String, text: String) {
        if (ENABLED) {
            Log.e(tag, text)
        }
    }

    fun e(text: String) {
        if (ENABLED) {
            Log.e(tag, text)
        }
    }

}